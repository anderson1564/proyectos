#define LED 40
#define LED1 41
#define LED2 42

#define BUZZER 49
#define PULSE A0
#define TEMPERATURE A2

#define ALARM_TIME 250ul
#define ALARM_CICLES 6ul

#define REST_ONE_GRADE_C 500.0/1023.0

#define MIN_PRES 900.0
#define TIME_PRES 5000
#define PRESS true
#define RELESED false

int cont_pres=0;
boolean state_pres =  RELESED;
unsigned long presure_time = 0ul;

void setup() {
  Serial.begin(9600);
  pinMode(TEMPERATURE, INPUT);
  pinMode(LED, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(PULSE, INPUT);
}

void loop() {
  Serial.print(F("Press/time: "));
  int count = checkPresure();
  Serial.print(count);
  Serial.print(F(" Temp: "));
  Serial.print(getTemperature());
  if(count > 3){
    initAlarm();
    Serial.print(F(" Alarm"));
  }
  Serial.println();
}

int checkPresure(){
  //Read presure
  float presure = analogRead(PULSE);
  //Change the state to realease if presure less that minimun
  if(state_pres==PRESS && presure > MIN_PRES){
    state_pres=RELESED;
  }
  
  //Init or update the count
  if(state_pres == RELESED && presure <= MIN_PRES){
    state_pres = PRESS;
    //If press first time, init count
    if(cont_pres==0){
      presure_time = millis() + TIME_PRES;
    }
    ++cont_pres;
  }
  
  //End the counter
  if(millis()>presure_time){
    int temp_cont_pres = cont_pres;
    cont_pres = 0;
  }
  return cont_pres;
}

float getTemperature(){
  return float(analogRead(TEMPERATURE) * REST_ONE_GRADE_C);
}

void initAlarm(){
  unsigned long short_end_time = millis() + ALARM_TIME;
  unsigned long long_end_time = millis() + (ALARM_CICLES*ALARM_TIME);
  while(millis() < long_end_time){
    digitalWrite(LED, HIGH);
    digitalWrite(LED1, HIGH);
    digitalWrite(LED2, HIGH);
    digitalWrite(BUZZER, HIGH);
    while(millis() < short_end_time);
    digitalWrite(LED, LOW);
    digitalWrite(LED, LOW);
    digitalWrite(LED, LOW);
    digitalWrite(BUZZER, LOW);
    short_end_time = millis() + ALARM_TIME;
    while(millis() < short_end_time);
    short_end_time = millis() + ALARM_TIME;
  }
}
