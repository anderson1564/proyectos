#define IN_SEN_RED 53
#define IN_SEN_BLUE 51
#define LIMIT_TIME 1500
#define DEBOUNCE_TIME 50
#define IN_PERSON 1
#define OUT_PERSON -1

char sensorRed;
char sensorBlue;
char sensorRed_old;
char sensorBlue_old;
long contPeople;
unsigned long dataSensorRed;
unsigned long dataSensorBlue;
unsigned long actualTime;

void calibracion();
void updateContPeople(int);

void setup(){
  //Set the serial monitor
  Serial.begin(9600);
  //Set the input mode to the pins
  pinMode(IN_SEN_RED, INPUT);
  pinMode(IN_SEN_BLUE, INPUT);
  //Let the current time over the limit time
  delay(LIMIT_TIME);
}

//Update people cont
void updateContPeople(int i){
  contPeople+=i;
  Serial.println(contPeople);
  dataSensorRed=0;
  dataSensorBlue=0;
}

void calibration(){
  if(sensorRed)
    Serial.print("--RED-- ");
  if(sensorBlue)
    Serial.print("--Blue--");
  if(sensorBlue|sensorRed)
    Serial.println();
}

void loop(){
  sensorRed = digitalRead(IN_SEN_RED);
  sensorBlue = digitalRead(IN_SEN_BLUE);
  //Init the counter when out of the range of the sensor
  if(sensorRed_old && sensorRed==0)
    dataSensorRed=millis();
  if(sensorBlue_old && sensorBlue==0)
    dataSensorBlue=millis();
  actualTime = millis();
  if(actualTime-dataSensorBlue<LIMIT_TIME && actualTime-dataSensorRed<LIMIT_TIME){
    //Update if the person out
    if(sensorBlue_old && sensorBlue==0)
      updateContPeople(OUT_PERSON);
     //Update if the person in
    if(sensorRed_old && sensorRed==0)
      updateContPeople(IN_PERSON);
    //Delay for avoid bounce of the signal
    delay(DEBOUNCE_TIME);
  }
  //Save the state of the sensors for the next loop
  sensorRed_old=sensorRed;
  sensorBlue_old=sensorBlue;
  //Uncomment the next line to calibrate the sensors
  //calibration();
}
