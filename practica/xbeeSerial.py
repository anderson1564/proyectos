#!/usr/bin/python

import sys
from serial import Serial, SerialException
from datetime import datetime

DEBUG = False
fileName = 'serialXbee'
serialPort = '/dev/ttyUSB0'

def readSerialToFile():
    """Read a serial port and save the data to a file"""
    try:
        try:
            ser = Serial(serialPort, 9600)
        except SerialException:
            print 'No connected device'
            exit()
        while(True):
           out = ser.readline()
           print out
    except KeyboardInterrupt:
        ser.close()
        print '\nOut by keyboard'

def main():
    """Main function"""
    readSerialToFile()
    
if __name__ == "__main__":
    main()
