#!/usr/bin/python

import sys
import threading
from serial import Serial, SerialException
from datetime import datetime

file_out = 'rawOutput'+str(datetime.now())
SERIAL_PORT = ['/dev/ttyACM0', '/dev/ttyUSB0']
NUMBER_THREADS_READER = 2
DEBUG = False
MUTEX = threading.RLock()
contPeopleThreads = [None]*NUMBER_THREADS_READER
outFile = None

def updateLine(line, t_id):
    #Omit the empty lines
    if line=='\r\n':
        return False
    contPeopleThreads[t_id] = line[:-2]
    return True

def write_file():
    var=0
    try:
        outFile = open(file_out, 'a')
        line=str(datetime.now())
        total=0
        for cont in range(NUMBER_THREADS_READER):
            var = contPeopleThreads[cont]
            if(var==None):
                var = 0
            total = total+int(var)
            line+=' '+ str(var)
        line+=' '+ str(total)+'\n'
        if DEBUG: print repr(line)
        outFile.writelines(line)
    except IOError:
        print 'I/O Error'
    except ValueError:
        print "Error parsing to number: " + var
    finally:
        outFile.close()

class ThreadReader (threading.Thread):
    def __init__(self, serialPort, t_id):
        threading.Thread.__init__(self)
        self.serialPort = serialPort
        self.t_id = t_id
        self.kill_received = False
    def run(self):
        try:
            self.ser = Serial(self.serialPort, 9600)
            while not self.kill_received:
                self.line = self.ser.readline()
                updateLine(self.line, self.t_id)
                MUTEX.acquire()
                write_file()
                MUTEX.release()
        except SerialException:
            print 'No device connected'
        finally:
            self.ser.close()

def main():
    """Main function"""
    outFile = open(file_out,'w')
    threads = []
    for cont in range(NUMBER_THREADS_READER):
        serialPort = SERIAL_PORT[cont]
        th = ThreadReader(serialPort, cont)
        threads.append(th)
        th.start()
    raw_input('Press enter to close')
    for t in threads:
        t.kill_received = True
    
if __name__ == "__main__":
    main()
