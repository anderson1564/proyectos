#!/bin/sh
help(){
    echo "Default option: -i"
    echo "Kali options:"
    echo "\t-i, --init"
    echo "\t-h, --help"
    echo "\t-c, --connect" 
    echo "\t-s, --stop\n"
    echo "Vulnerable server options:"
    echo "\t-v, --vulnerable"
    echo "\t-sv, --stop-vulnerable"
}

connect(){
    echo "Connecting kali..."
    ssh root@172.16.44.132
}

stop(){
    echo "Stoping Kali"
    vmrun -T player stop /home/suser/vmware/Kali\ Linux\ 1.0.6\ 64\ bit/Kali\ Linux\ 1.0.6\ 64\ bit.vmx
    echo -e "\a"
}

init(){
    echo "Starting kali..." && 
    vmrun -T player start /home/suser/vmware/Kali\ Linux\ 1.0.6\ 64\ bit/Kali\ Linux\ 1.0.6\ 64\ bit.vmx nogui && 
    echo "Waiting for connecting..." && 
    sleep 9 && 
    connect
}

start_vulnerable(){
    echo "Starting vulnerable"
    vmrun -T player start /home/suser/vmware/Metasploitable2-Linux/Metasploitable.vmx &&
    echo "vulnerable running"
}

stop_vulnerable(){
    echo "Stopping vulnerable"
    vmrun -T player stop /home/suser/vmware/Metasploitable2-Linux/Metasploitable.vmx &&
    echo "vulnerable stoped"
}

case "$1" in
    -h|--help)
        help
        ;;
    -c|--connect)
        connect
        ;;
    -s|--stop)
        stop
        ;;
    -v|--vulnerable)
        start_vulnerable
        ;;
    -sv|--stop-vulnerable)
        stop_vulnerable
        ;;
    -i|--init|*)
        init
        ;;
esac
