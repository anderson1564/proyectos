import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Anderson LM
 */
public class CommandLinuxWC {
    
    // Enum for the boolean flags
    private enum Flag{LINES, WORDS, CHARS, BYTES, MAX_LINE_LENGTH};
    private final String prefix = "--files0-from=";
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CommandLinuxWC comLinuxWC = new CommandLinuxWC();
        comLinuxWC.parseArgs(args);
    }
    
    /**
     * Contructor CommandLinuxWC
     */
    public CommandLinuxWC(){}
    
    /**
     * Parse and process the input like the linux command wc
     * @param args
     */
    public void parseArgs(String[] args){
        boolean haveFlags = false;
        String arrFiles [] = null;
        //Number of possible flags
        int numFlags = Flag.values().length;
        boolean arrBools[] = new boolean[numFlags];
        for (int position=0; position< args.length; ++position){
            String arg = args[position];
            if(arg.startsWith(prefix)){
                if(position != args.length-1){
                    System.out.println("CommandLinuxWC: extra operator <<"+arg+">>");
                    System.exit(0);
                }
                if(position == 0)
                    haveFlags = true;
                String path = arg.replaceFirst(prefix, "");
                List<String> listFiles;
                if(path.equalsIgnoreCase("-")){
                    listFiles = processStandarInput();
                }else{
                    listFiles = readFile(path);
                }
                arrFiles = listFiles.toArray(new String[0]);
            }
            else{
                switch(arg){
                    case "--bytes":
                    case "-c":
                        arrBools[Flag.BYTES.ordinal()] = true;
                        break;
                    case "--chars":
                    case "-m":
                        arrBools[Flag.CHARS.ordinal()] = true;
                        break;
                    case "--lines":
                    case "-l":
                        arrBools[Flag.LINES.ordinal()] = true;
                        break;
                    case "--max-line-length":
                    case "-L":
                        arrBools[Flag.MAX_LINE_LENGTH.ordinal()] = true;
                        break;
                    case "--words":
                    case "-w":
                        arrBools[Flag.WORDS.ordinal()] = true;
                        break;
                    case "--help":
                        help();
                        return;
                    case "--version":
                        version();
                        return;
                    default:
                        //Set havaFlags diffenrent to zero if valid flags
                        int numFiles = args.length-position; 
                        arrFiles = new String[numFiles];
                        System.arraycopy(
                                    args, position, arrFiles, 0 ,numFiles);
                        if(position == 0)
                            haveFlags = true;    
                        position = args.length;
                }
            }
        }
        
        if(haveFlags || args.length == 0){
            // Options by default
            arrBools[Flag.LINES.ordinal()] = true;
            arrBools[Flag.WORDS.ordinal()] = true;
            arrBools[Flag.BYTES.ordinal()] = true;
        }
        
        List<int[]> lResults = new ArrayList<>();
        if(arrFiles != null){
            for (String file : arrFiles) {
                List<String> lReadLines = readFile(file, true);
                int result[] = wcLines(lReadLines, arrBools);
                lResults.add(result);
            }
        }else{
            List<String> lReadLines = processStandarInput();
            int result[] = wcLines(lReadLines, arrBools);
            lResults.add(result);
        }
        printResults(lResults, arrBools, arrFiles);
    }
    
    private void printResults(List<int[]> lResults, boolean[] arrBools, 
            String[] arrFiles){
        if(arrFiles!= null){
            int arrTotals[]= new int [arrBools.length];
            int cont = 0;
            for (int[] result : lResults) {
                int i;
                for(i=0; i<arrTotals.length-1; ++i){
                    arrTotals[i] += result[i];
                }
                arrTotals[i] = calMaxLineLength(arrTotals[i], result[i]);
                printValues(result, arrBools, arrFiles[cont++]);
            }
            if(arrFiles.length > 1){
                printValues(arrTotals, arrBools, "total");
            }
        }else{
            printValues(lResults.get(0), arrBools, "");
        }
    }
    
    private void printValues(int arrInt[], boolean[] arrBools, String name){
        for (int i = 0; i < arrInt.length; i++) {
            if(arrBools[i])
                System.out.print(arrInt[i] +" ");
        }
        System.out.println(name);
    }
    
    /**
     * Read the lines without '/n' and return a list with all the lines
     * @param filePatch
     * @return 
     */
    private List<String> readFile(String filePatch){
        return readFile(filePatch, false);
    }
    /**
     * Read the lines with or without '/n' and return a list with all the lines
     * @param filePatch
     * @param addNewLine
     * @return 
     */
    private List<String> readFile(String filePatch, boolean addNewLine){
        List<String> listLines = new ArrayList<>();
        try
        {
            String inputLine = null;
            //Open the file input
            FileInputStream fInStream = new FileInputStream(filePatch);
            try (DataInputStream in = new DataInputStream(fInStream)) {
                BufferedReader br = new BufferedReader(
                                            new InputStreamReader(in));
                boolean fLine = false;
                while ((inputLine = br.readLine()) != null)   {
                    if(fLine && addNewLine){
                        listLines.add(
                                listLines.remove(listLines.size()-1)+'\n');
                    }
                    listLines.add(inputLine);
                    fLine = true;
                }
            }   
        } 
        catch (IOException ex) {
            System.err.println("CommandLinuxWC: " + filePatch + 
                                ": Error reading the file");
            System.exit(0);
        }
        return listLines;
    }
    
    /**
     * Count the number of lines, words, chars, bytes or/and 
     * calculate the line with more characters
     * @param listLines
     * @param arrBools
     * @return 
     */
    private int[] wcLines(List<String> listLines, boolean[] arrBools){
        int numLines = 0, numBytes = 0, numChars = 0, numWords = 0; 
        int numMaxLineLength = 0;
        for (String strLine : listLines) {  
            if(arrBools[Flag.LINES.ordinal()])
                numLines += countNewLines(strLine);
            if(arrBools[Flag.WORDS.ordinal()])
                numWords += calcWordsLine(strLine);
            if(arrBools[Flag.CHARS.ordinal()])
                numChars += calcCharsLine(strLine);
            if(arrBools[Flag.MAX_LINE_LENGTH.ordinal()])
                numMaxLineLength = calMaxLineLength(strLine, numMaxLineLength);
            try{
                if(arrBools[Flag.BYTES.ordinal()])
                    numBytes += calcBytesLine(strLine);
            }
            catch (UnsupportedEncodingException e){
                System.err.println("Error calculating number of bytes "+
                        "unsupported encoding utf8");
                System.exit(0);
            }
        }
        int arrResults[] = 
                {numLines, numWords, numChars, numBytes, numMaxLineLength};
        return arrResults;
    }
    
    /**
     * Read console input by lines and return a list of lines
     * @return 
     */
    private List<String> processStandarInput(){
        String inputLine = null;
        List<String> listLines = new ArrayList<>();
        while(true){
            inputLine = System.console().readLine();
            if(inputLine == null)
                break;
            else
                listLines.add(inputLine);
        }
        return listLines;        
    }
    
    /**
     * Take the length of two lines and return the highest
     * @param line1
     * @param line2
     * @return 
     */
    private int calMaxLineLength(String line1, int line2){
        int actualLineLength = line1.replace("\n", "").length();
        if(actualLineLength > line2){
            return actualLineLength;
        }
        return line2;
    }
    
    /**
     * Take the length of two lines and return the highest
     * @param line1
     * @param line2
     * @return 
     */
    private int calMaxLineLength(int line1, int line2){
        return (line1 > line2)? line1 : line2;
    }
    
    /**
     * Calculate the number os bytes of a line
     * @param line
     * @return
     * @throws UnsupportedEncodingException 
     */
    private int calcBytesLine(String line) throws UnsupportedEncodingException{
        byte[] arrBytes = line.getBytes("UTF-8");
        return arrBytes.length;
    }
    
    /**
     * Calculate the number of chars of a line
     * @param line
     * @return 
     */
    private int calcCharsLine(String line) {
        return line.length();
    }
    
    /**
     * Calculate the number of words of a line
     * @param line
     * @return 
     */
    private int calcWordsLine(String line) {
        //if lines is empty return 0 otherwise return the number of words
        return line.split(" ").length;
    }
    
    private int countNewLines(String line){
        return line.contains("\n") ? 1 : 0;
    }
    
    /**
     * Print the version of the application
     */
    private void version() {
        System.out.println(
                "Command Linux WC Java v 1.0\nWritten by Anderson LM");
    }
    
    /**
     * Print the help
     */
    private void help() {
        System.out.println(strHelp);
    }
    
    private final static String strHelp = 
"NAME\n" +
"       CommandLinuxWC - print newline, word, and byte counts for each file\n" +
"\n" +
"SYNOPSIS\n" +
"       CommandLinuxWC [OPTION]... [FILE]...\n" +
"       CommandLinuxWC [OPTION]... --files0-from=F\n" +
"\n" +
"DESCRIPTION\n" +
"       Print  newline, word, and byte counts for each FILE, and a total line "
            + "if more than one FILE is specified.  With no FILE, or when FILE "
            + "is -,\n" +
"       read standard input.  A word is a non-zero-length sequence of "
            + "characters delimited by white space.  The options below may be "
            + "used to select\n" +
"       which counts are printed, always in the following order: newline, word,"
            + " character, byte, maximum line length.\n" +
"\n" +
"       -c, --bytes\n" +
"              print the byte counts\n" +
"\n" +
"       -m, --chars\n" +
"              print the character counts\n" +
"\n" +
"       -l, --lines\n" +
"              print the newline counts\n" +
"\n" +
"       --files0-from=F\n" +
"              read input from the files specified by NUL-terminated names in "
            + "file F; If F is - then read names from standard input\n" +
"\n" +
"       -L, --max-line-length\n" +
"              print the length of the longest line\n" +
"\n" +
"       -w, --words\n" +
"              print the word counts\n" +
"\n" +
"       --help display this help and exit\n" +
"\n" +
"       --version\n" +
"              output version information and exit\n" +
"\n" +
"AUTHOR\n" +
"       Written by Anderson Lopez Monsalve.\n" +
"       November 2013\n" +
"\n" +
"ORIGINAL VERSION\n" +
"       wc (GNU coreutils) 8.20\n" +
"       Written by Paul Rubin and David MacKenzie.";
}