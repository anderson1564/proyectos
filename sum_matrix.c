/*
   File: suma_matriz.c
   Date: 21 oct 2013
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 1024
#define DEBUG 0

int matriz[N][N];
int sum[N];

int addMatriz(int *m){
  int j;
  int s = 0;
  for(j=0; j<N; ++j)
    s += m[j];
  return s;
}

int correctPosition(int* i, int* j,int n){
  if(*j>=n){
    *j=0;
    *i+=1;
    if(*i>=n)
      return 0;
  }
  return 1;
}

int loadFileInMatriz(const char filename[]){
  int i=0;
  FILE *file = fopen ( filename, "r" );
  if ( file != NULL )
  {
    char line [ 128 ];
    int j=0;
    while (correctPosition(&i, &j, N) &&
           fgets ( line, sizeof line, file ) != NULL)
    {
      matriz[i][j++]=atoi(line);
    }
    fclose ( file );
  }
  else
  {
    perror ( filename ); /* why didn't the file open? */
    return 1;
  }
  return 0;
}

int main(void) {
  clock_t start = clock();
  if(loadFileInMatriz("input")){
    exit(0);
  }
  int i=0;
  for(i=0; i<N; ++i){
    sum[i] = addMatriz(matriz[i]);
    if(DEBUG) printf("matriz[%d]: %d ",i,sum[i]);
  }

  clock_t end = clock();

  double time_elapsed_in_seconds = (end - start)/(double)CLOCKS_PER_SEC;
  printf("Total time %lf",time_elapsed_in_seconds);
  return 0;
}
