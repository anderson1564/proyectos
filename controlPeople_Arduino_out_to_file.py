#!/usr/bin/python

import sys
from serial import Serial, SerialException
from datetime import datetime

DEBUG = False
fileName = 'rawOutput'
serialPort = '/dev/ttyACM0'

def readSerialToFile():
    """Read a serial port and save the data to a file"""
    try:
        try:
            ser = Serial(serialPort, 9600)
        except SerialException:
            print 'No connected device'
            exit()
        try:
            out = ''
            outFile = open(fileName,'w')
            while True:
                out = ser.readline()
                #Omit the empty lines
                if out=='\r\n':
                    continue
                #Remove the /n of out and concatenate the current time
                out=out[:-1]+'  '+str(datetime.now())
                outFile.writelines(out)
                if DEBUG: print repr(out)
        except IOError:
            print 'I/O Error'
        finally:
            outFile.close()
    except KeyboardInterrupt:
        ser.close()
        print '\nOut by keyboard'

def main():
    """Main function"""
    readSerialToFile()
    
if __name__ == "__main__":
    main()
