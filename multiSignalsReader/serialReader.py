import serial
import time
from time import gmtime, strftime
import json
import sys, os
import traceback

conf={}
#directory
directory='data'
#Serial port to use
serialPort = '/dev/ttyACM0'
#OPTIONAL time of each read
conf['time']=1000
#Pins arduino to read (Arduino Mega [0..15] )

corr1='SenCorri_1_5amp'
corr2='SenCorri_2_30amp'
corr3='SenCorri_3_30amp'
Temp1='Temp1'
Temp2='Temp2'
Temp3='Temp3'
Temp4='Temp4'

arrRead={
  corr1 : 0,
  corr2 : 1,
  corr3 : 2,
  Temp1 : 3,
  Temp2 : 4,
  Temp3 : 5,
  Temp4 : 6,
  'Vol' : 7
}

corr = {corr1, corr2, corr3};
temp = {Temp1, Temp2, Temp3, Temp4}

def trans_data(data):
  for k, v in arrRead:
    for dc in corr:
      if k == dc:
        data[v]= trans_corr(data[v], 1024.0, 5.0)
        break
    for dt in temp:
      if k == dt:
        data[v]= trans_temp(data[v], 1024.0, 5.0)
  return data

def trans_corr(v, i1, i2):
  data = float((i2/i1)*(v))
  return data

def trans_temp(v):
  return (v*100)-1

def main():
  readCicle = True
  wFile=None
  ser=None
  try:
    ser = serial.Serial(serialPort, 9600, timeout=1)
    if ser.isOpen():
      #Print the serial port
      print ser.name
      time.sleep(1)
      #Set the pins to read
      conf['data']=getArrValues(arrRead)
      #Create folder and file to storage the data
      wFile = createFile()
      ser.write(json.dumps(conf))
      i=0
      while readCicle:
        try:
          rData =json.loads(ser.readline())
          if i==0:
            header = printHeaders(rData)
            saveLineOnFile(wFile, header)
            i=i+1
          pData = printData(rData)
          saveLineOnFile(wFile, pData)
        except ValueError:
          pass
  except KeyboardInterrupt:
    readCicle = False
    if ser!= None:
      ser.close()
    if wFile!= None:
      wFile.close()
    print "\n\nEnd\n"
  except serial.serialutil.SerialException:
    if ser!= None:
      ser.close()
    if wFile!= None:
      wFile.close()
    print 'Error en la salida serial'
    print 'Esperando para reconexion'
    wait_time = 10
    for i in range(wait_time):
      print (wait_time-i)
      time.sleep(1)
    main()
  except Exception:
      if ser!= None:
        ser.close()
      if wFile!= None:
        wFile.close()
      print 'Error Inesperado: ', sys.exc_info()[0]
      traceback.print_exc()
      print 'Esperando para reconexion'
      wait_time = 10
      for i in range(wait_time):
        print (wait_time-i)
        time.sleep(1)
      main()

################## Arduino #########################

def getArrValues(arr):
  data=[]
  for item, value in arr.iteritems():
    data.append(value);
  return data

def getItem(arr):
  for item, value in arr:
    print item;

def arrToStr(arr, type):
  strArr = ''
  if type=='k':
    for key, value in arr.iteritems():
      strArr = strArr +"\t"+str(key)
  elif type=='v':
    for key, value in arr.iteritems():
      strArr = strArr +"\t"+str(value)
  #remove the first tab
  return strArr[1:]

def printHeaders(headers):
  header = ''
  for item in headers:
    i=0
    for key, value in arrRead.iteritems():
      if str(item) == str(value):
        header = header+key+":"+item+"\t"
        break
    if i==1:
      header = header+"-\t"
  header = header+'\tTime\tDate\n'
  print header
  return header

def printData(jData):
  jData = trans_data(jData)
  outData = arrToStr(jData, 'v')\
    +'\t'+ strftime("%H:%M:%S %Y-%m-%d", gmtime())
  print outData
  return outData

################### End Arduino ####################

################### File ###########################

def createFile():
  if not os.path.exists(directory):
    os.makedirs(directory)
  nameFile = directory+"/"+directory+"_"+strftime("%H:%M:%S %Y-%m-%d", gmtime())
  return open(nameFile, 'a')

def saveLineOnFile(wFile, line):
  wFile.write(line+'\n')

################### End File #######################

if __name__ == "__main__":
  main()
  

