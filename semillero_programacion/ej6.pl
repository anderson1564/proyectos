num(1, ' one ' ).
num(2, ' two ' ).
num(3, ' three ' ).
num(4, ' four ' ).
num(5, ' five ' ).
num(6, ' six ' ).
num(7, ' seven ' ).
num(8, ' eight ' ).
num(9, ' nine ' ).

num(11, ' eleven ' ).
num(12, ' twelve ' ).
num(13, ' threeteen ' ).
num(14, ' fourteen ' ).
num(15, ' fiveteen ' ).
num(16, ' sixteen ' ).
num(17, ' seventeen ' ).
num(18, ' eighteen ' ).
num(19, ' nineteen ' ).

num(10, ' ten ' ).
num(20, ' twenty ' ).
num(30, ' thirty ' ).
num(40, ' fourty ' ).
num(50, ' fifty ' ).
num(60, ' sixty ' ).
num(70, ' seventy ' ).
num(80, ' eighty ' ).
num(90, ' ninety ' ).

super_num(0,'').
super_num(1,'').
super_num(2, ' hundred ' ).
super_num(3, ' thousand ' ).
super_num(4,'').
super_num(5,'').
super_num(6, ' million ' ).
super_num(7,'').
super_num(8,'').
super_num(9, ' billion ' ).
super_num(10,'').
super_num(11,'').
super_num(12, ' trillion ' ).
super_num(13,'').
super_num(14,'').
super_num(15, ' quadrillion ' ).
super_num(16,'').
super_num(17,'').
super_num(18, ' quintillion ' ).
super_num(19,'').
super_num(20,'').
super_num(21, ' sextillion ' ).
super_num(22,'').
super_num(23,'').
super_num(24, ' septillion ' ).
super_num(25,'').
super_num(26,'').
super_num(27, ' octillion ' ).
super_num(28,'').
super_num(29,'').
super_num(30, ' nonillion ' ).
super_num(31,'').
super_num(32,'').
super_num(33, ' decillion ' ).
super_num(34,'').
super_num(35,'').
super_num(36, ' sextillion ' ).
super_num(37,'').
super_num(38,'').
super_num(39, ' duodecillion ' ).
super_num(40,'').
super_num(41,'').
super_num(42, ' tredecillion ' ).

str_two_nums(N,0,STR) :-
    num(N,STR).
str_two_nums(N,STR) :-
    N1 is mod(N,10),
    N3 is (N//100) * 100,
    N2 is N - N1 - N3,
    num(N2,FNUM),
    num(N1,SNUM),
    atomic_concat(FNUM, SNUM, STR).

str_thousan_num(0,_,''):- !.
str_thousan_num(N,STR) :-
    NN is (N//100),
    str_two_nums(NN,FNUM),
    super_num(3,SNUM),
    atomic_concat(FNUM, SNUM, STR).

num_3(0,''):-!.
%%Convert numbers 10^X with X between 2 and 42
num_3(N,STR) :-
    num(N,FNUM),
    super_num(2,SNUM),
    atomic_concat(FNUM, SNUM, STR).

num_2_1(0,_,'') :- !.
num_2_1(N,_,STR) :-
    num(N,STR),
    !.
%%Convert numbers between 1 and 99
num_2_1(N,_,STR):-
    N1 is mod(N,10),
    N2 is mod(N,100) - N1,
    num(N2,FNUM),
    num(N1,SNUM),
    atomic_concat(FNUM, SNUM, STR).

title_num('',_,'').
%%Put the title of the number, Example: 100 -> 'one hundred'
title_num(NUM,CONT,STR):-
    super_num(CONT,TITLE),
    atomic_concat(NUM, TITLE, STR).

str_3_num(0,_,'') :- !.
%%Recive the last 3 numbers and convert
str_3_num(N,CONT,STR) :-
    NN is (N//100),
    MOD is mod(N,100),
    num_3(NN,FNUM),
    num_2_1(MOD,CONT,SNUM),
    atomic_concat(FNUM, SNUM, NUM),
    title_num(NUM,CONT,STR).

%%Trick zero
str_nums(0,0,STR):-
    atomic_concat('zero', '', STR),
    !.
str_nums(0,_,''):- !.
%%Take the last 3 numbers an convert and repit until no more numbers
str_nums(N,CONT,STR) :-
    NEXT_CONT is CONT + 3,
    NEXT_N is div(N,1000),
    str_nums(NEXT_N,NEXT_CONT,FNUM),
    MOD is mod(N,1000),
    str_3_num(MOD,CONT,SNUM),
    atomic_concat(FNUM, SNUM, STR).

convert_num(N) :-
    str_nums(N,0,STR),
    write(STR).
