/*
   File: overlaod_operator.cpp
   Date: 28 sep 2013
 */

#include <iostream>

using namespace std;

class Element{
public:
  int value;
  Element (int value);
  Element  operator+(int value);
  Element  operator-(int value);
  Element  operator=(Element  value);
  Element  operator=(int value);
};

Element::Element (int value){
  this->value = value;
}

Element Element::operator= (Element e){
  this->value =  e.value;
  return (*this);
}

Element Element::operator= (int value){
  this->value = value;
  return (*this);
}

Element Element::operator+ (int value){
  this->value += value;
  return (*this);
}

Element Element::operator- (int value){
  this->value -= value;
  return (*this);
}

void put(Element e){
  cout <<"Value: "<< e.value << endl;
}

int main()
{
  Element e (5);
  Element f (10);
  e = 25;
  put(e);
  e = e + 7;
  put(e);
  e = e - 7;
  put(e);
  e = f;
  put(e);
  return 0;
}
