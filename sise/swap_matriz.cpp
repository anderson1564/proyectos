/* 
   File: swap_matriz.cpp
   Date: 31 ago 2013
 */

#include <iostream>
#include <cstdlib>

using namespace std;

#define SIZE_M 7

void make_matriz_2d(int***, int);
void init_matriz_2d(int***, int,int);
void print_matriz_2d(int***, int);

void init_matriz_2d(int***o_matriz, const int size_m, const int num=0){
  int **matriz = *o_matriz;
  for(int i=0; i<size_m; ++i)
    for(int j=0; j<size_m; ++j)
      matriz[i][j]=num;
}

void make_matriz_2d(int*** o_matriz, const int size_m){
  int **matriz = (int**) malloc(size_m*sizeof(int*));
  for (int i = 0; i < size_m; i++)
    (matriz)[i] = (int*) malloc(size_m*sizeof(int));
  *o_matriz = matriz;
}

void print_matriz_2d(int*** matriz, const int size_m){
  for(int i=0; i<size_m; ++i){
    for(int j=0; j<size_m; ++j)
      cout << (*matriz)[i][j] << "\t";
    cout << "\n";
  }
}

int main()
{
  int** ma,**mb,**mt=NULL;
  make_matriz_2d(&ma, SIZE_M);
  make_matriz_2d(&mb, SIZE_M);
  init_matriz_2d(&ma, SIZE_M);
  init_matriz_2d(&mb, SIZE_M,1);
  cout << "Matriz A\n";
  print_matriz_2d(&ma, SIZE_M);
  cout << "Matriz B\n";
  print_matriz_2d(&mb, SIZE_M);
  cout << "\nSwap [A-B]\n\n";

  mt = ma;
  ma = mb;
  mb = mt;

  cout << "Matriz A\n";
  print_matriz_2d(&ma, SIZE_M);
  cout << "Matriz B\n";
  print_matriz_2d(&mb, SIZE_M);  
}
