###  README.md 
## El juego de la vida

#Intructions
	make clean
	make
	./el_juego_de_la_vida

#Function : Init the pattern
Change the function init the pattern

#Rules:
	1. Any live cell with fewer than two live neighbours dies, as if caused by under-population.
  	2. Any live cell with two or three live neighbours lives on to the next generation.
  	3. Any live cell with more than three live neighbours dies, as if by overcrowding.
  	4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

	http://en.wikipedia.org/wiki/Conway's_Game_of_Life

## Keywords: 
	game, life, pointers, matriz
## Files: 
	el_juego_de_la_vida.cpp
	Makefile
## Date: 
	1 September 2013
## Author: 
	Anderson L M






