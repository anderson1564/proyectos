/* 
   File: el_juego_de_la_vida.cpp
   Date: 1 sep 2013
   Autor: Anderson L M
 */

/* 
  Rules:
  1. Any live cell with fewer than two live neighbours dies, as if caused by under-population.
  2. Any live cell with two or three live neighbours lives on to the next generation.
  3. Any live cell with more than three live neighbours dies, as if by overcrowding.
  4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
  http://en.wikipedia.org/wiki/Conway's_Game_of_Life
*/

#include <iostream>
#include <cstdlib>

using namespace std;

#ifdef __linux
  #ifndef LINUX  
    #define LINUX
  #endif
#else
  #undef LINUX
#endif

#ifdef LINUX

#include <chrono>
#include <thread>

#define T_SLEEP 2000

void sleep_time(int time){
    chrono::milliseconds dura(time);
    this_thread::sleep_for( dura );
}
#endif 

//Variables
#define DEAD 0
#define LIVE 1
int** actual_gen = NULL;
int** next_gen = NULL;

//Matriz funcions
void make_matriz_2d(int***, int);
void print_matriz_2d(int***, int);
void init_matriz_2d(int***, int,int);
void init_pattern(int***);

//Game life functions
void life(int);
int ck(int,int);
//Validate if cell is dead or live
int is_life(int ,int ,int);

//Functions
void init_matriz_2d(int***o_matriz, const int size_m, const int num=0){
  int **matriz = *o_matriz;
  for(int i=0; i<size_m; ++i)
    for(int j=0; j<size_m; ++j)
      matriz[i][j]=num;
}

void make_matriz_2d(int*** o_matriz, const int size_m){
  int **matriz = (int**) malloc(size_m*sizeof(int*));
  for (int i = 0; i < size_m; i++)
    (matriz)[i] = (int*) malloc(size_m*sizeof(int));
  *o_matriz = matriz;
}

void print_matriz_2d(int*** matriz, const int size_m){
  cout << __func__ << endl;
  for(int i=0; i<size_m; ++i){
    for(int j=0; j<size_m; ++j)
      cout << (*matriz)[i][j] << "\t";
    cout << "\n";
  }
}

int ck(const int x,const int size_m){
  return (x ==-1) ? size_m-1 : x==size_m ? 0 : x; 
}

int is_life(const int i,const int j,const int sum){
//Rules
  if((actual_gen[i][j] && sum == 2) ||  (sum == 3))
    return LIVE;
  return DEAD;
}

void life(const int s){
  int sum;
  for(int i=0; i<s; ++i){
    for(int j=0; j<s; ++j){
      sum = 
	actual_gen[ck(i-1,s)][ck(j-1,s)] + 
	actual_gen[ck(i-1,s)][ck(j,s)] + 
	actual_gen[ck(i-1,s)][ck(j+1,s)] + 
	actual_gen[ck(i,s)][ck(j-1,s)] + 
	actual_gen[ck(i,s)][ck(j+1,s)] + 
	actual_gen[ck(i+1,s)][ck(j-1,s)] + 
	actual_gen[ck(i+1,s)][ck(j,s)] + 
	actual_gen[ck(i+1,s)][ck(j+1,s)];
      next_gen[i][j] = is_life(i, j, sum);
    }
  }
}

void init_pattern(int*** matriz){
  (*matriz)[3][1] =LIVE;
  (*matriz)[3][2] =LIVE;
  (*matriz)[3][3] =LIVE;
  (*matriz)[3][4] =LIVE;
  (*matriz)[3][5] =LIVE;
}

int main()
{
  int size_m = 7, **temp=NULL;
  //Make the matriz
  make_matriz_2d(&actual_gen, size_m);
  make_matriz_2d(&next_gen, size_m);
  //Init the matriz
  init_matriz_2d(&actual_gen, size_m);
  //Init the start pattern
  init_pattern(&actual_gen);
  
  for(int i=0; i<5; ++i){
    print_matriz_2d(&actual_gen, size_m);
    //When the magic happends
    life(size_m);
    //Swap matriz
    temp = actual_gen;
    actual_gen = next_gen;
    next_gen = temp;
    //Sleep 2 segs in each iteraction if is linux
    #ifdef LINUX
      sleep_time(T_SLEEP);
    #endif
  }
}

#undef T_SLEEP
#undef LINUX
#undef LIVE
#undef DEAD

