class Position{
  int x,y;

  Position(int x, int y){
      this.x=x;
      this.y=y;
  }
  
  void set_new_post(int x, int y){
    this.x=x;
    this.y=y;
  }
  
  int  get_x(){
    return this.x;
  }
  
  int  get_y(){
    return this.y;
  }
}
