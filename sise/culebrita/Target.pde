class Target{
  int x, y, cols, rows, with, higth;
  boolean active;
  
  Target(int with, int higth, int cols, int rows){
    this.cols=cols;
    this.rows=rows;
    this.with=with;
    this.higth=higth;
    this.active=false; 
  }
  
  int[][] destroy_target(int arr[][]){
    arr[y][x]=0;
    active=false;
    return arr;
  }
  
  int[][] new_target(int arr[][]){
    if(!active){
      //set random position
      x=int(random(0, cols));
      y=int(random(0, rows));
      //Repit until find a valid position
      while(arr[y][x]!=0){
        x=int(random(0, cols));
        y=int(random(0, rows));
      }
      //Mark valid target position 
      arr[y][x]=3; 
      //Active target
      active=true;
    }
    return arr;
  }
  
  void update(){
    if(active){
      fill(200,0,0);
      rect(x*with, y*higth, with, higth);
    }
  }
}
