class Snake{
  int pos_x, pos_y, cols, rows, move, live;
  int w_snake, h_snake;
  ArrayList<Body> snake;
  int arr_snake[][];
  Target target;
  
  Snake(int w_snake, int h_snake,int cols, int rows){
    this.w_snake=w_snake;
    this.h_snake=h_snake;
    this.cols=cols;
    this.rows=rows;
  }
  
  void update(){
    direction_moving();
    move_snake();
  }
  
  void new_snake(){
    live=1;
    move=4;
    pos_x=0;
    pos_y=0;
    snake = new ArrayList<Body>();
    this.arr_snake = new int[rows][cols];
    add_body(cols/2, rows/2, true);
    add_body((cols/2)-1, rows/2, false);
    add_body((cols/2)-2, rows/2, false);
    reset_target();
  }
  
  void add_body(int x, int y, boolean head){
    snake.add(new Body(x, y, w_snake, h_snake, head));
    arr_snake[y][x] = head ? 1 : 2;
  }
  
  void reset_target(){
    arr_snake=target.destroy_target(arr_snake);
    arr_snake=target.new_target(arr_snake);
  }
  
  boolean between(int point, int border){
    return (point>=0 && point < border);
  }
  
  int is_live(int x, int y){
    if(between(x, cols) 
      && between(y, rows) 
      && (arr_snake[y][x]==0 || arr_snake[y][x]==3)){
        return live;
    }
    return 0;
  }
  
  void move_snake(){
    //Create temporal varaibles
    int old_x=0, old_y=0;
    int new_x, new_y;
    Body body=null;
    for(int i=0; i<snake.size(); ++i){
      body = snake.get(i);
      if(body.is_head()){
        //update the old positions
        old_x = body.get_x();
        old_y = body.get_y();
        //update the new positions
        new_x = pos_x + old_x;
        new_y = pos_y + old_y;
        //Check if can move to the new position
        this.live = is_live(new_x,new_y);
        //Check if need add a new body part
        check_add_body(new_x,new_y, old_x, old_y);
        //set new position of the head
        body.set_new_pos(new_x, new_y);
        if(this.live==0){
          break;
        }
        //Mark the head in the array positions
        arr_snake[new_y][new_x]=2;
      }else{
        //update the new positions
        new_x = body.get_x();
        new_y = body.get_y();
        //set new position of the body part
        body.set_new_pos(old_x, old_y);
        //update the old positions
        old_x = new_x;
        old_y = new_y;
        //Mark the body part in the array positions
        arr_snake[new_y][new_x]=1;
      }
      body.update();
    }
    //Erase the last past mark ofthe final body part
    arr_snake[old_y][old_x]=0;
  }
  
  void check_add_body(int head_x, int head_y,int body_x, int body_y){
    //Check if catch a target
    if(this.live != 0 && check_target(head_x, head_y)){
      reset_target();
      add_body(body_x, body_y, false);
      //this increases the speed
      ++live;
    }
  }
  
  boolean check_target(int x, int y){
    return arr_snake[y][x]==3;
  }
  
  void direction_moving(){
    //up 1, down 2,  left 3, rigth 4
    pos_x=0; 
    pos_y=0;
    switch(move) {
      case 1: 
        pos_y=1;
        break;
      case 2: 
        pos_y=-1;
        break;
      case 3:
        pos_x=-1;
        break;
      case 4:
        pos_x=1;
        break;
    }
  }
  
  void set_target(Target target){
    this.target=target;
  }
  
  void set_move(int move){
    this.move=move;
  }
  
  void set_arr_snake(int[][] arr_snake){
    this.arr_snake=arr_snake;
  }
  
  int get_move(){
    return this.move;
  }

  int get_live(){
    return live;
  }
  
  int[][] get_arr_snake(){
    return arr_snake;
  }
}
