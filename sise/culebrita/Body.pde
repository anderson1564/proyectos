class Body{
  int w,h;
  Position pos, old_pos;
  boolean head = false;
  
  Body(int x, int y, int w, int h, boolean head){
    this.pos = new Position(x,y);
    this.head = head;
    this.w = w;
    this.h = h;
  }
  
  void update(){
    if(is_head())
      fill(0,0,255);  
    else
      fill(255);
    rect(pos.get_x()*w, pos.get_y()*h, w, h);  
  }
  
  void set_new_pos(int new_x, int new_y){
    pos.set_new_post(new_x, new_y);
  }
  
  void set_head(boolean head){
    this.head = head;
  }
  
  boolean is_head(){
    return head;
  }
  
  int get_x(){
    return pos.get_x();
  }
  
  int get_y(){
    return pos.get_y();
  }
}
