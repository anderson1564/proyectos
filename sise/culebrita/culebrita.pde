final int WITH_SNAKE=25, HIGTH_SNAKE=25;
final int COL=40, ROW=20;
final int WITH=COL*WITH_SNAKE, HIGTH=ROW*HIGTH_SNAKE;
int frame_rate=3, level=1;
boolean can_move;
Snake snake;
Target target;

void setup() {
  size(WITH, HIGTH);
  snake = new Snake(WITH_SNAKE,HIGTH_SNAKE,COL,ROW);
  target = new Target(WITH_SNAKE,HIGTH_SNAKE,COL,ROW);
  snake.set_target(target);
  snake.new_snake();
  can_move = true;
  frameRate(frame_rate);
}

void draw() {
 background(255, 204, 0);
 if(snake.get_live()!=0){
   snake.update();
   target.update();
   check_level();
  }else{
    textSize(50);
    fill(255, 0, 0);
    text("DEAD", WITH/2-50, HIGTH/2-70);
    textSize(20);
    text("Press R", WITH/2+20, HIGTH/2-40);
    snake.set_arr_snake(target.destroy_target(snake.get_arr_snake()));
  }
  can_move = true;
}

void check_level(){
  int t_level = snake.get_live();
  if(t_level != level){
    level = t_level==0 ? 0 : t_level;
    frameRate(frame_rate+level);
  }
}

void no_can_move(){
  can_move = false;
}

void keyPressed() {
    //up 1, down 2,  left 3, rigth 4
    if(can_move == true){
      switch(keyCode) {
        case 40: 
          if(snake.get_move()!=2){
            snake.set_move(1);
            no_can_move();
          }
          break;
        case 38: 
          if(snake.get_move()!=1){
            snake.set_move(2);
            no_can_move();
          }
          break;
        case 37: 
          if(snake.get_move()!=4){
            snake.set_move(3);
            no_can_move();
          }
          break;
        case 39: 
          if(snake.get_move()!=3){
            snake.set_move(4);
            no_can_move();
          }
          break;
        case 'r': case 'R':
          if(snake.get_live()==0){
            snake.new_snake();
          }
          break;
        }
    }
}
